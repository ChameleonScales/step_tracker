import bpy
#from mathutils import Vector

class CLIP_OT_setup_clips(bpy.types.Operator) :
    bl_idname = "clip.setup"
    bl_label = "Setup clips"
    bl_description = "Copies the active clip in order to dump all tracks for faster solving"
    bl_options = {'INTERNAL'}

    def execute(self, context) :
        global dump_clip
        global solve_clip
        solve_clip = bpy.context.area.spaces[0].clip
        dump_clip = bpy.context.area.spaces[0].clip.copy()
        dump_clip.name = solve_clip.name + '_Dump'
        dump_clip.use_fake_user = True
        solve_clip.name += '_Solve'

        return {'FINISHED'}

class CLIP_OT_step_track(bpy.types.Operator) :
    bl_idname = "clip.step_track"
    bl_label = "Track one step"
    bl_description = "Track one step of specified duration"
    bl_options = {'INTERNAL'}

    def execute(self, context) :
        Sc = bpy.context.scene
        Props = Sc.ST_props
        Sl = Props.step_duration
        bpy.context.area.spaces[0].clip = solve_clip
        clip = bpy.context.edit_movieclip
        clip.tracking.tracks.data.settings.default_frames_limit = Sl

        bpy.context.area.spaces[0].show_disabled = True
        c_op = bpy.ops.clip
        c_op.clear_solution()
        c_op.select_all(action = 'SELECT')
        c_op.hide_tracks(unselected = False)
        c_op.detect_features(placement = 'FRAME',
                                     margin = Props.detect_margin,
                                     threshold = Props.detect_threshold,
                                     min_distance = Props.detect_min_distance
                                     )
        c_op.track_markers(backwards = False, sequence = True)
        c_op.filter_tracks(track_threshold = Props.filter_threshold)
        c_op.delete_track()
        c_set = solve_clip.tracking.settings
        c_set.use_keyframe_selection = True
        c_op.solve_camera()
        c_op.clean_tracks(error = Props.reproj_error, action = 'DELETE_SEGMENTS')

        c_set = solve_clip.tracking.settings
        c_set.use_keyframe_selection = False
        c_set.refine_intrinsics_focal_length = False
        c_set.refine_intrinsics_principal_point = False
        c_set.refine_intrinsics_radial_distortion = False

        c_op.solve_camera()
        c_op.clean_tracks(error = Props.reproj_error, action = 'DELETE_SEGMENTS')
        c_op.select_all(action = 'SELECT')
        c_op.copy_tracks()
        bpy.context.area.spaces[0].clip = dump_clip
        c_op.paste_tracks()
        bpy.context.area.spaces[0].clip = solve_clip
        c_op.delete_track()
        next_start = Sc.frame_current + Props.frame_offset
        Sc.frame_current = next_start
        bpy.context.area.spaces[0].clip_user.frame_current = next_start
        bpy.context.area.spaces[0].clip = dump_clip
        return {'FINISHED'}

class CLIP_OT_batch_step_track(bpy.types.Operator) :
    bl_idname = "clip.batch_step_track"
    bl_label = "Track Multiple Steps"
    bl_description = "Runs the 1 step operator multiple times"
    bl_options = {'INTERNAL'}

    def execute(self, context) :
        Sc = bpy.context.scene
        Props = Sc.ST_props
        steps = Props.batch_steps
        start_frame = Sc.frame_current
        wm = bpy.context.window_manager
        wm.progress_begin(0, steps)
        for n in range(steps):
            #bpy.ops.clip.refresh()
            bpy.ops.clip.step_track()
            #next_start = start_frame + (n * Props.step_duration) #+ Props.frame_offset
            #bpy.context.area.spaces[0].clip_user.frame_current = next_start
            #bpy.ops.clip.refresh()
            wm.progress_update(n)
        wm.progress_end()
        return {'FINISHED'}

class CLIP_OT_clear_trackless(bpy.types.Operator) :
    bl_idname = "clip.clear_trackless"
    bl_label = "Select Segmentless"
    bl_description = "Select all tracks with no tracking segment"
    bl_options = {'INTERNAL'}

    def execute(self, context) :
        tracking = bpy.context.area.spaces[0].clip.tracking
        for tr in tracking.tracks :
            if len(tr.markers) <= 3 :
                tr.select = True

        return {'FINISHED'}

CLASSES = [CLIP_OT_setup_clips,
           CLIP_OT_step_track,
           CLIP_OT_batch_step_track,
           CLIP_OT_clear_trackless]

def register():
    for cls in CLASSES:
        bpy.utils.register_class(cls)

def unregister():
    for cls in CLASSES:
        bpy.utils.unregister_class(cls)
