# Step Tracker

### Blender add-on to track movie clip motion by steps of n frames.

#### This add-on is especially useful for long shots where the environment is constantly evolving.

# How to use:

⚠ Note that both tracking operators dump all the added trackers in a copy of your Movie clip. This is necessary for better performance. You can find your trackers in the clip copy containing "_Dump" in its name.

**⚠ It is highly advised to first [calibrate your camera](https://docs.blender.org/manual/en/latest/movie_clip/tracking/introduction.html#manual-lens-calibration).**

1. In the User Preferences > 'System' tab, increase the Memory cache limit as much as possible
2. Open your clip in the Movie Clip Editor
3. In the left panel (T) > 'Track' tab:
    - <kbd>Set Scene Frames</kbd>
    - <kbd>Prefetch</kbd>
    - adjust parameters under Tracking Settings (& Extra)
4. in the 'Solve' tab, adjust the parameters under "Solve"
5. In the right panel (N), 'Step Tracking' tab, click on <kbd>Setup clips</kbd>
6. Adjust the parameters above the <kbd>Track one step</kbd> button
7. To track one step, click on that button
8. To track multiple steps at once, set the number of steps below and click on the <kbd>Track multi steps</kbd> button

**⚠ Reminder:**
if you have these checkboxes activated :
```
Refine:
✅ Focal Length
✅ Optical Center
✅ Radial Distortion
```
then make sure they don't diverge into crazy values (right panel > Track > Camera) and un-check them when you have good values.


## 🗈 Todo:
- expose more internal operations
- add option to skip failed steps
- provide clear failure messages
- detect frames with not enough overlapping segments to prevent the solver from stopping there
