# ##### BEGIN GPL LICENSE BLOCK #####
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published by
#    the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
	"name": "step tracker",
	"description": "batch track motion by steps of n frames",
	"author": "ChameleonScales (chameleonscales@protonmail.com)",
	"version": (0, 1),
	"blender": (2, 92, 0),
	"location": "Movie Clip Editor > Side Panel (N) > Step Tracker tab",
	"doc_url": "https://gitlab.com/ChameleonScales/step_tracker/",
	"tracker_url": "https://gitlab.com/ChameleonScales/step_tracker/-/issues",
	"category": "Video Tools" }

import bpy
from bpy.props import (IntProperty,
                       FloatProperty,
                       PointerProperty)
from . import sidePanel, operatorsList
#from .operatorsList import CLIP_OT_step_track
#from .sidePanel import STEP_TRACKER_PT_ui


class StepTrackerProperties(bpy.types.PropertyGroup):

    step_duration = IntProperty(
        name = "Step Duration (frames)",
        description = "Will track for this duration",
        default = 28,
        )

    detect_margin = IntProperty(
        name = "Detection Margin",
        description = "ignore image borders by n pixels",
        default = 40,
        )

    detect_threshold = FloatProperty(
        name = "Detection Threshold",
        description = "Min contrast level of feature. Higher value adds less trackers",
        default = 0.5,
        )

    detect_min_distance = IntProperty(
        name = "Detection Distance",
        description = "Min distance between trackers",
        default = 20,
        )

    filter_threshold = FloatProperty(
        name = "Filter threshold",
        description="Delete trackers with spiky curves. Higher value keeps more trackers",
        default = 12,
        )

    reproj_error = FloatProperty(
        name = "Reprojection error",
        description="threshold used by cleanup operator. Higher value keeps more tracks",
        default = 2,
        )

    frame_offset = IntProperty(
        name = "Frame offset",
        description="Offset the current frame at the end of the operation",
        default = -14
		,
        )

    batch_steps = IntProperty(
        name = "No. of steps",
        description = "Number of times to execute the 1 step operator",
        default = 6,
        )


CLASSES = [StepTrackerProperties]

def register():
    for cls in CLASSES:
        bpy.utils.register_class(cls)
    bpy.types.Scene.ST_props = PointerProperty(type=StepTrackerProperties)
    operatorsList.register()
    sidePanel.register()

def unregister():
    sidePanel.unregister()
    operatorsList.unregister()
    del bpy.types.Scene.ST_props
    for cls in CLASSES:
        bpy.utils.unregister_class(cls)
