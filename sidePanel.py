import bpy

class STEP_TRACKER_PT_ui(bpy.types.Panel):
    bl_label = "Step tracking"
    bl_space_type = "CLIP_EDITOR"
    bl_region_type = "UI"
    bl_category = "Step Tracker"
    #bl_context = "mesh_edit"

    def draw(self, context):
        layout = self.layout
        ST = context.scene.ST_props

        split = layout.split(factor = 0.23, align=True)
        left_col = split.column(align = True)
        right_col = split.column(align = True)
        right_col.scale_y = 2
        right_col.operator('clip.setup')
        layout.prop(ST, 'step_duration')

        col = layout.column(align=True)
        col.prop(ST, 'detect_margin')
        col.prop(ST, 'detect_threshold')
        col.prop(ST, 'detect_min_distance')

        layout.prop(ST, 'filter_threshold')

        layout.prop(ST, 'reproj_error')

        layout.prop(ST, 'frame_offset')

        split = layout.split(factor = 0.23, align=True)
        left_col = split.column(align = True)
        right_col = split.column(align = True)
        right_col.scale_y = 2
        right_col.operator('clip.step_track', text='Track one step', icon='TRACKING_FORWARDS_SINGLE')

        layout.separator(factor=2.0)
        layout.prop(ST, 'batch_steps')
        split = layout.split(factor = 0.23, align=True)
        left_col = split.column(align = True)
        right_col = split.column(align = True)
        right_col.scale_y = 2
        right_col.operator('clip.batch_step_track', text='Track multi steps', icon='SEQ_LUMA_WAVEFORM')
        right_col.separator(factor=2.0)
        right_col.operator('clip.clear_trackless', text='Select Segmentless', icon='TRACKER')

CLASSES = [STEP_TRACKER_PT_ui]

def register():
    for cls in CLASSES:
        bpy.utils.register_class(cls)

def unregister():
    for cls in CLASSES:
        bpy.utils.unregister_class(cls)
